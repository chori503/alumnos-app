package sv.edu.usam.portal_notas.DTO;

import java.util.ArrayList;
import java.util.List;

public class AlumnoHorarioActualDTO {
	private AlumnoDTO alumno;
	private ArrayList<HorarioDTO> horarios;

	public AlumnoHorarioActualDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AlumnoHorarioActualDTO(AlumnoDTO alumno, ArrayList<HorarioDTO> horarios) {
		super();
		this.alumno = alumno;
		this.horarios = horarios;
	}

	public AlumnoDTO getAlumno() {
		return alumno;
	}

	public void setAlumno(AlumnoDTO alumno) {
		this.alumno = alumno;
	}

	public ArrayList<HorarioDTO> getHorarios() {
		return horarios;
	}

	public void setHorarios(ArrayList<HorarioDTO> horarios) {
		this.horarios = horarios;
	}

}
