package sv.edu.usam.portal_notas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sv.edu.usam.portal_notas.DTO.HorarioDTO;
import sv.edu.usam.portal_notas.R;

public class ListaHorarioDisponibleAdapter extends RecyclerView.Adapter<ListaHorarioDisponibleAdapter.ViewHolder> {
    private ArrayList<HorarioDTO> dataset;
    Context context;

    public ListaHorarioDisponibleAdapter(Context context) {
        this.context = context;
        dataset = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horario_disponible, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       /* Pokemon pkm=dataset.get(position);
        holder.tvPokeNombre.setText(pkm.getName());
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pkm.getNumber()+".png")
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgPokemon);
    */
        HorarioDTO horarioDTO = dataset.get(position);
        holder.tvHaNombreMateria.setText(horarioDTO.getMateria().getNombre());
        holder.tvHaDiaMateria.setText(horarioDTO.getDia());
        holder.tvHaHoraInicio.setText(horarioDTO.getHoraInicio());
        holder.tvHahoraFin.setText(horarioDTO.getHoraFin());
        holder.tvNombreCiclo.setText(horarioDTO.getCiclo().getNombre());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void agregarListaHorario(ArrayList<HorarioDTO> horarioDTOS) {
        dataset.addAll(horarioDTOS);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // ImageView imgPokemon;
        TextView tvHaNombreMateria, tvHaDiaMateria, tvHaHoraInicio, tvHahoraFin,tvNombreCiclo;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //imgPokemon = (ImageView) itemView.findViewById(R.id.imgePokemon);
            //tvPokeNombre = (TextView) itemView.findViewById(R.id.tvNombrePoke);
            tvHaNombreMateria=(TextView) itemView.findViewById(R.id.tvHdNombreCiclo);
            tvHaDiaMateria=(TextView) itemView.findViewById(R.id.tvHdDia);
            tvHaHoraInicio=(TextView) itemView.findViewById(R.id.tvHdHoraInicio);
            tvHahoraFin=(TextView) itemView.findViewById(R.id.tvHdHoraFin);
            tvNombreCiclo=(TextView) itemView.findViewById(R.id.tvHdNombreCiclo);

        }
    }
}
