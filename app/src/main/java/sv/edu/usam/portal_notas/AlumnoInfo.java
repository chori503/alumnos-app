package sv.edu.usam.portal_notas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sv.edu.usam.portal_notas.DTO.AlumnoDTO;
import sv.edu.usam.portal_notas.DTO.HorarioDTO;
import sv.edu.usam.portal_notas.DTO.ResponseDTO;
import sv.edu.usam.portal_notas.adapters.ListaHorarioDisponibleAdapter;
import sv.edu.usam.portal_notas.apiClient.PortalAlumnoWsRetroFit;
import sv.edu.usam.portal_notas.utils.Utils;

public class AlumnoInfo extends AppCompatActivity {
    ListView listView;
    Adapter adapter;
    RecyclerView recyclerView;
    //ListaHorarioDisponibleAdapter listaHorarioDisponibleAdapter;
    Button btnVolver;
    Retrofit retrofit;
    TextView tvPrimerNombre,tvSegundoNombre,tvPrimeApellido,tvSegundoApellido,tvCarnet,tvDui,tvMail,tvFechaNac,tvTelefono,tvCarrera;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_info);
        tvPrimerNombre=findViewById(R.id.Primernombre);
        tvSegundoNombre=findViewById(R.id.segundoNombre);
        tvPrimeApellido=findViewById(R.id.primerApellido);
        tvSegundoApellido=findViewById(R.id.segundoApellido);
        tvCarnet=findViewById(R.id.txtCarnet);
        tvDui=findViewById(R.id.txtDui);
        tvMail=findViewById(R.id.txtMail);
        tvFechaNac=findViewById(R.id.txtFechaNacimiento);
        tvTelefono=findViewById(R.id.txtTelefono);
        tvCarrera=findViewById(R.id.txtCarrera);
        // builder and passing our base url
        retrofit = new Retrofit.Builder()
                .baseUrl(Utils.URL_PORTAL_ALUMNO_WS)
                // as we are sending data in json format so
                // we have to add Gson converter factory
                .addConverterFactory(GsonConverterFactory.create())
                // at last we are building our retrofit builder.
                .build();
        info();
    }
    public void info(){
        PortalAlumnoWsRetroFit alumnoWsRetroFit = retrofit.create(PortalAlumnoWsRetroFit.class);
        SharedPreferences prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
        String carnet = prefs.getString("carnet", ""); // prefs.getString("nombre del campo" , "valor por defecto")
        Log.e("CARNET", carnet);
        Call<ResponseDTO> responseHorarios = alumnoWsRetroFit.alumnoInfo(carnet);

        responseHorarios.enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, retrofit2.Response<ResponseDTO> response) {
                Log.i("HORARIO ACTUAL", "SUCCESS: " + response.body());

                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("00")) {
                        //ArrayList<HorarioDTO> horarios = Utils.mapper.convertValue(response.body().getResponse(), ArrayList<HorarioDTO>().class);
                        AlumnoDTO alumnoDTO = Utils.mapper.convertValue(response.body().getResponse(), AlumnoDTO.class);
                    tvPrimerNombre.setText(alumnoDTO.getPrimerNombre());
                    tvSegundoNombre.setText(alumnoDTO.getSegundoNombre());
                    tvPrimeApellido.setText(alumnoDTO.getPrimerApellido());
                    tvSegundoApellido.setText(alumnoDTO.getSegundoApellido());
                    tvCarnet.setText(tvCarnet.getText()+": "+alumnoDTO.getCarnet());
                        tvDui.setText(tvDui.getText()+": "+alumnoDTO.getDui());
                        tvFechaNac.setText(tvFechaNac.getText()+": "+alumnoDTO.getFechaNacimiento());
                        tvTelefono.setText(tvTelefono.getText()+": "+alumnoDTO.getTelefono());
                        tvMail.setText(tvMail.getText()+": "+alumnoDTO.getMail());
                        tvCarrera.setText(tvCarrera.getText()+": "+alumnoDTO.getCarrera());

                    }
                } else {
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.body());
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("HORARIO ACTUAL", "onFailure: " + t.getMessage());
            }
        });
    }
    public void alumnoInfo(View view){

    }

    public void  horarioActual(View view){
        startActivity(new Intent(getApplicationContext(),HorariosActuales.class));
        finish();
    }

    public void  horarioDisponible(View view){
        startActivity(new Intent(getApplicationContext(),HorariosDisponibles.class));
        finish();
    }
    public void  pagos(View view){
        startActivity(new Intent(getApplicationContext(),Pagos.class));
        //finish();
    }
    public void  login(View view){
        startActivity(new Intent(getApplicationContext(),Login.class));
        //finish();
    }
}