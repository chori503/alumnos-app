package sv.edu.usam.portal_notas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sv.edu.usam.portal_notas.DTO.AlumnoHorarioActualDTO;
import sv.edu.usam.portal_notas.DTO.ResponseDTO;
import sv.edu.usam.portal_notas.adapters.ListaHorarioActualAdapter;
import sv.edu.usam.portal_notas.apiClient.PortalAlumnoWsRetroFit;
import sv.edu.usam.portal_notas.utils.Utils;

public class HorariosActuales extends AppCompatActivity {
    ListView listView;
    Adapter adapter;
    RecyclerView recyclerView;
    ListaHorarioActualAdapter listaHorarioActualAdapter;
    public static ArrayList<HorarioActualLista> horarioActualListaArrayList = new ArrayList<>();
    //Button btnVolver;
    String url = "http://192.168.1.3:8080/alumnos-ws/horarios/{carnet}/actual";
    HorarioActualLista horarios;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horarios_actuales);

        recyclerView = findViewById(R.id.rvHorarioActual);
        listaHorarioActualAdapter = new ListaHorarioActualAdapter(this);
        recyclerView.setAdapter(listaHorarioActualAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        // builder and passing our base url
        retrofit = new Retrofit.Builder()
                .baseUrl(Utils.URL_PORTAL_ALUMNO_WS)
                // as we are sending data in json format so
                // we have to add Gson converter factory
                .addConverterFactory(GsonConverterFactory.create())
                // at last we are building our retrofit builder.
                .build();
        horariosAlumno();
       /* btnVolver=findViewById(R.id.btnHaVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(),Home.class);
                startActivity(intent);
            }
        });*/
    }

    public void horariosAlumno() {
        PortalAlumnoWsRetroFit alumnoWsRetroFit = retrofit.create(PortalAlumnoWsRetroFit.class);
        SharedPreferences prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
        String carnet = prefs.getString("carnet", ""); // prefs.getString("nombre del campo" , "valor por defecto")
        Log.e("CARNET", carnet);
        Call<ResponseDTO> responseHorarios = alumnoWsRetroFit.horariosActuales(carnet);

        responseHorarios.enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, retrofit2.Response<ResponseDTO> response) {
                Log.i("HORARIO ACTUAL", "SUCCESS: " + response.body());

                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("00")) {
                        AlumnoHorarioActualDTO horarioActualDTO = Utils.mapper.convertValue(response.body().getResponse(), AlumnoHorarioActualDTO.class);
                        listaHorarioActualAdapter.agregarListaHorario(horarioActualDTO.getHorarios());
                    }
                } else {
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.body());
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("HORARIO ACTUAL", "onFailure: " + t.getMessage());
            }
        });

    }

    public void horarioActual(View view) {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                horarioActualListaArrayList.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String exito = jsonObject.getString("exito");
                    JSONArray jsonArray = jsonObject.getJSONArray("horarios");
                    if (exito.equals("1")) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);

                            String id = object.getString("id");
                            String hora_inicio = object.getString("hora inicio");
                            String hora_fin = object.getString("hora fin");
                            String id_materia = object.getString("materia");
                            String dia = object.getString("dia ");
                            String id_ciclo = object.getString("ciclo");

                            horarios = new HorarioActualLista(id, hora_inicio, hora_fin, id_materia, dia, id_ciclo);
                            horarioActualListaArrayList.add(horarios);
                            adapter.notifyDataSetChanged();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    ;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HorariosActuales.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }


    public void  alumnoInfo(View view){
        startActivity(new Intent(getApplicationContext(),AlumnoInfo.class));
        finish();
    }

    public void  horarioDisponible(View view){
        startActivity(new Intent(getApplicationContext(),HorariosDisponibles.class));
        finish();
    }
    public void  pagos(View view){
        startActivity(new Intent(getApplicationContext(),Pagos.class));
        //finish();
    }
    public void  login(View view){
        startActivity(new Intent(getApplicationContext(),Login.class));
        //finish();
    }
}