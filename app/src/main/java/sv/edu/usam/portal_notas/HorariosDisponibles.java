package sv.edu.usam.portal_notas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sv.edu.usam.portal_notas.DTO.AlumnoHorarioActualDTO;
import sv.edu.usam.portal_notas.DTO.HorarioDTO;
import sv.edu.usam.portal_notas.DTO.ResponseDTO;
import sv.edu.usam.portal_notas.adapters.ListaHorarioActualAdapter;
import sv.edu.usam.portal_notas.adapters.ListaHorarioDisponibleAdapter;
import sv.edu.usam.portal_notas.apiClient.PortalAlumnoWsRetroFit;
import sv.edu.usam.portal_notas.utils.Utils;

public class HorariosDisponibles extends AppCompatActivity {
    ListView listView;
    Adapter adapter;
    RecyclerView recyclerView;
    ListaHorarioDisponibleAdapter listaHorarioDisponibleAdapter;
    Button btnVolver;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horarios_disponibles);

        recyclerView = findViewById(R.id.rvHorarioDisponible);
        listaHorarioDisponibleAdapter = new ListaHorarioDisponibleAdapter(this);
        recyclerView.setAdapter(listaHorarioDisponibleAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        // builder and passing our base url
        retrofit = new Retrofit.Builder()
                .baseUrl(Utils.URL_PORTAL_ALUMNO_WS)
                // as we are sending data in json format so
                // we have to add Gson converter factory
                .addConverterFactory(GsonConverterFactory.create())
                // at last we are building our retrofit builder.
                .build();
        horariosFuturos();
        btnVolver = findViewById(R.id.btnHdVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
            }
        });
    }

    public void horariosFuturos() {
        PortalAlumnoWsRetroFit alumnoWsRetroFit = retrofit.create(PortalAlumnoWsRetroFit.class);
        SharedPreferences prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
        String carnet = prefs.getString("carnet", ""); // prefs.getString("nombre del campo" , "valor por defecto")
        Log.e("CARNET", carnet);
        Call<ResponseDTO> responseHorarios = alumnoWsRetroFit.horarioDisponible();

        responseHorarios.enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, retrofit2.Response<ResponseDTO> response) {
                Log.i("HORARIO ACTUAL", "SUCCESS: " + response.body());

                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("00")) {
                        //ArrayList<HorarioDTO> horarios = Utils.mapper.convertValue(response.body().getResponse(), ArrayList<HorarioDTO>().class);
                        List<HorarioDTO> horarios = Utils.mapper.convertValue(response.body().getResponse(), new TypeReference<List<HorarioDTO>>() {
                        });
                        ArrayList<HorarioDTO> hlist= (ArrayList<HorarioDTO>) horarios;
                        Log.e("LIST", hlist.get(0).getDia());

                        listaHorarioDisponibleAdapter.agregarListaHorario((ArrayList<HorarioDTO>) horarios);
                    }
                } else {
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.body());
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("HORARIO ACTUAL", "onFailure: " + t.getMessage());
            }
        });

    }

    public void horarioDisponible(View view) {

    }

    public void  horarioActual(View view){
        startActivity(new Intent(getApplicationContext(),HorariosActuales.class));
        finish();
    }

    public void  alumnoInfo(View view){
        startActivity(new Intent(getApplicationContext(),AlumnoInfo.class));
        finish();
    }

    public void  pagos(View view){
        startActivity(new Intent(getApplicationContext(),Pagos.class));
        //finish();
    }
    public void  login(View view){
        startActivity(new Intent(getApplicationContext(),Login.class));
        //finish();
    }
}