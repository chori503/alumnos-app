package sv.edu.usam.portal_notas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sv.edu.usam.portal_notas.DTO.HorarioDTO;
import sv.edu.usam.portal_notas.DTO.PagosDTO;
import sv.edu.usam.portal_notas.DTO.ResponseDTO;
import sv.edu.usam.portal_notas.adapters.ListaHorarioDisponibleAdapter;
import sv.edu.usam.portal_notas.adapters.ListaPagoAdapter;
import sv.edu.usam.portal_notas.apiClient.PortalAlumnoWsRetroFit;
import sv.edu.usam.portal_notas.utils.Utils;

public class Pagos extends AppCompatActivity {
    ListView listView;
    Adapter adapter;
    RecyclerView recyclerView;
    ListaPagoAdapter listaPagoAdapter;
   // Button btnVolver;
    Retrofit retrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagos);

        recyclerView = findViewById(R.id.rvPagos);
        listaPagoAdapter = new ListaPagoAdapter(this);
        recyclerView.setAdapter(listaPagoAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        // builder and passing our base url
        retrofit = new Retrofit.Builder()
                .baseUrl(Utils.URL_PORTAL_ALUMNO_WS)
                // as we are sending data in json format so
                // we have to add Gson converter factory
                .addConverterFactory(GsonConverterFactory.create())
                // at last we are building our retrofit builder.
                .build();
        pagos();
       /* btnVolver = findViewById(R.id.btnPVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
            }
        });*/
    }
    public void pagos(){
        PortalAlumnoWsRetroFit alumnoWsRetroFit = retrofit.create(PortalAlumnoWsRetroFit.class);
        SharedPreferences prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
        String carnet = prefs.getString("carnet", ""); // prefs.getString("nombre del campo" , "valor por defecto")
        Log.e("CARNET", carnet);
        Call<ResponseDTO> responseHorarios = alumnoWsRetroFit.pagosPorAlumno(carnet);

        responseHorarios.enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, retrofit2.Response<ResponseDTO> response) {
                Log.i("HORARIO ACTUAL", "SUCCESS: " + response.body());

                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("00")) {
                        //ArrayList<HorarioDTO> horarios = Utils.mapper.convertValue(response.body().getResponse(), ArrayList<HorarioDTO>().class);
                        List<PagosDTO> pagosDTOS = Utils.mapper.convertValue(response.body().getResponse(), new TypeReference<List<PagosDTO>>() {
                        });
//                        ArrayList<HorarioDTO> hlist= (ArrayList<HorarioDTO>) horarios;
                        Log.e("LIST", pagosDTOS.get(0).getId().toString());

                        listaPagoAdapter.agregarListPago((ArrayList<PagosDTO>) pagosDTOS);
                    }
                } else {
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.body());
                    Log.e("HORARIO ACTUAL", "MAL RESPONSE: " + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("HORARIO ACTUAL", "onFailure: " + t.getMessage());
            }
        });
    }
    public void  horarioActual(View view){
        startActivity(new Intent(getApplicationContext(),HorariosActuales.class));
        finish();
    }

    public void  alumnoInfo(View view){
        startActivity(new Intent(getApplicationContext(),AlumnoInfo.class));
        finish();
    }

    public void  horarioDisponible(View view){
        startActivity(new Intent(getApplicationContext(),HorariosDisponibles.class));
        finish();
    }
    public void  login(View view){
        startActivity(new Intent(getApplicationContext(),Login.class));
        //finish();
    }
}