package sv.edu.usam.portal_notas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import sv.edu.usam.portal_notas.DTO.HorarioDTO;
import sv.edu.usam.portal_notas.DTO.PagosDTO;
import sv.edu.usam.portal_notas.R;

public class ListaPagoAdapter extends RecyclerView.Adapter<ListaPagoAdapter.ViewHolder> {
    private ArrayList<PagosDTO> dataset;
    Context context;

    public ListaPagoAdapter(Context context) {
        this.context = context;
        dataset = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pagos, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       /* Pokemon pkm=dataset.get(position);
        holder.tvPokeNombre.setText(pkm.getName());
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pkm.getNumber()+".png")
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgPokemon);
    */
        PagosDTO pago = dataset.get(position);
        holder.tvPmes.setText(nombreMes(pago.getNumMes()));
        holder.tvPfechaPago.setText(pago.getFechaPago());
        holder.tvPmonto.setText(String.valueOf(pago.getCuota()));
        holder.tvPcticlo.setText(pago.getCiclo().getNombre());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void agregarListPago(ArrayList<PagosDTO> pagosDTOS) {
        dataset.addAll(pagosDTOS);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // ImageView imgPokemon;
        TextView tvPmes, tvPfechaPago, tvPmonto, tvPcticlo;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //imgPokemon = (ImageView) itemView.findViewById(R.id.imgePokemon);
            //tvPokeNombre = (TextView) itemView.findViewById(R.id.tvNombrePoke);
            tvPmes=(TextView) itemView.findViewById(R.id.tvPmes);
            tvPfechaPago=(TextView) itemView.findViewById(R.id.tvPfecha);
            tvPmonto=(TextView) itemView.findViewById(R.id.tvPmonto);
            tvPcticlo=(TextView) itemView.findViewById(R.id.tvPciclo);
        }
    }
    private String nombreMes(int mes){
        String[] meses={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        return meses[mes+1];
    }
}
